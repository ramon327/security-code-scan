package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v2/project"
)

const (
	nugetCmd                    = "nuget"
	msbuildCmd                  = "msbuild"
	dotnetCmd                   = "dotnet"
	pkgSecurityCodeScan         = "SecurityCodeScan"
	flagSecurityCodeScanVersion = "security-code-scan-version"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   flagSecurityCodeScanVersion,
			Usage:  "Version of SecurityCodeScan",
			EnvVar: "SECURITY_CODE_SCAN_VERSION",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	filter, err := pathfilter.NewFilter(c)
	if err != nil {
		return nil, err
	}

	ioArr := []io.Reader{}

	for _, project := range findProjects(path, filter) {
		analyzeIo, err := analyzeProject(c, path, project)
		if err != nil {
			return nil, err
		}
		ioArr = append(ioArr, analyzeIo)
	}

	return ioutil.NopCloser(io.MultiReader(ioArr...)), nil
}

func analyzeProject(c *cli.Context, path string, proj string) (io.Reader, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		cmd.Dir = path
		return cmd
	}

	var buildCmd = dotnetCmd
	var finalArgs = []string{"build"}

	// Add SecurityCodeScan package
	var args = []string{"add", proj, "package", pkgSecurityCodeScan}
	if c.IsSet(flagSecurityCodeScanVersion) {
		args = append(args, "-v", c.String(flagSecurityCodeScanVersion))
	}

	if out, err := setupCmd(exec.Command(dotnetCmd, args...)).CombinedOutput(); err != nil {
		log.Errorf("Error:\n%s", out)
		log.Info("dotnet command failed, trying msbuild")
		if err := project.InsertAnalyzersToProjFile(proj); err != nil {
			return nil, err
		}
		buildCmd = msbuildCmd
		finalArgs = []string{path, "-t:Clean;Build", "-consoleLoggerParameters:NoSummary;Verbosity=minimal"}
	}

	if buildCmd == dotnetCmd {
		// Clean project
		if out, err := setupCmd(exec.Command(dotnetCmd, "clean", path)).CombinedOutput(); err != nil {
			log.Errorf("Error:\n%s", out)
			return nil, err
		}
	} else {
		if out, err := setupCmd(exec.Command(nugetCmd, "restore")).CombinedOutput(); err != nil {
			log.Errorf("Error nuget install:\n%s", out)
		}
	}

	// Build project and save output
	cmd := exec.Command(buildCmd, finalArgs...)
	cmd.Env = os.Environ()
	cmd.Dir = path
	cmd.Stderr = os.Stderr
	stdout, err := cmd.Output()
	if err != nil {
		// Ignore exit error
		if _, isExitErr := err.(*exec.ExitError); !isExitErr {
			return nil, err
		}
	}
	return bytes.NewBuffer(stdout), nil
}

func findProjects(dir string, filter *pathfilter.Filter) []string {
	projects := []string{}

	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		relPath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}

		if filter.IsExcluded(relPath) {
			if info.IsDir() {
				return filepath.SkipDir
			}

			return nil
		}

		switch filepath.Ext(path) {
		case ".csproj", ".vbproj":
			pathDir := filepath.Dir(path)
			if pathDir != dir {
				log.Debugf("Found project in %s\n", filepath.Dir(path))
			}

			projects = append(projects, path)
			return filepath.SkipDir
		default:
			// ignore
		}

		return nil
	})

	return projects
}
