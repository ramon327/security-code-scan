# security-code-scan .net analyzer changelog

## v2.13.0
- Update common and enabled disablement of rulesets (!58)

## v2.12.3
- Fix incorrect filepath for nested source files (!60)

## v2.12.2
- Update golang dependencies (!53)

## v2.12.1
- Use mono:6.10 container rather than mono:6.10-slim (!50)
- Update common to v2.16

## v2.12.0
- Update mono to v6.10 (!47)

## v2.11.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!35)

## v2.10.1
- Upgrade go to version 1.15 (!45)

## v2.10.0
- Add scan object to report (!42)

## v2.9.0
- Switch to the MIT Expat license (!39)

## v2.8.1
- Fix `findProjects` log-level from error to debug (!38)

## v2.8.0
- Update logging to be standardized across analyzers (!37)

## v2.7.3
- Fix misuse of `SAST_EXCLUDED_PATHS` resulting in scanned projects being skipped (!36)

## v2.7.2
- Change the location of custom CA certs (!32)

## v2.7.1
- Remove `location.dependency` from the generated SAST report (!31)

## v2.7.0
- Update [security-code-scan](https://security-code-scan.github.io/#3.5.3) to v3.5.3 (!30)

## v2.6.0
- Change Docker image base to use mono (!29 @agixid)
- Support building .NET Framework projects (!29 @agixid)

## v2.5.0
- Add `id` field to vulnerabilities in JSON report (!26)

## v2.4.0
- Add support for custom CA certs (!24)

## v2.3.0
- Add support for .NET multiprojects

## v2.2.0
- Upgrade to .NET Core SDK 3.1 (LTS) (!19 @shaun.burns)

## v2.1.1
- Update the `dotnet clean` command to run after the analyzer adds the security package

## v2.1.0
- Update [security-code-scan](https://security-code-scan.github.io/#3.3.0) to v3.3.0.

## v2.0.3
- Print stderr in case of execution error (!12)

## v2.0.2
- Bump common to v2.1.6

## v2.0.1
- Bump common to v2.1.5

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Upgrade to .NET SDK 2.2

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Show command error output

## v1.0.0
- initial release
